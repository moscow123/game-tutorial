pField=list(range(0, 9))
winCombinations=[(0,1,2),(3,4,5),(6,7,8),(0,4,8),(2,4,6),(0,3,6),(1,4,7),(2,5,8)]
#вычесть единицу
def drawBoard():
    for i in range(3):
        print("|", pField[0 + i * 3], "|", pField[1 + i * 3], "|""|", pField[2 + i * 3], "|")

def isPlaceCorrect(ntPlace1):
    return ntPlace1  in "012345678" and str(pField[int(ntPlace1)]) not in "XO"
    # if ntPlace not in "012345678":print("Введите число от одного до восьми")continue

def makeTurn(cPlayer):
    ntPlace="11"
    while not isPlaceCorrect(ntPlace):
        ntPlace = input("Куда хотите поставить " + str(cPlayer) + "")
    pField[int(ntPlace)]= cPlayer

def checkWin():
    for comb in winCombinations:
        if (pField[comb[0]])==(pField[comb[1]])==(pField[comb[2]]):
            return pField[comb[1]]
    else:
        return False

def main():
    winner=""
    for counter in range(8):
        drawBoard()
        if counter % 2 == 0:
            makeTurn("X")
        else:
            makeTurn("O")
        winner=checkWin()
        if winner:
            print(winner,"выиграл")
            exit(0)
    print("Ничья")
main()
