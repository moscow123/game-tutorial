
#nn
from tkinter import *
import tkinter
gameWidth=500
gameHeight=500
snake_item=10
snakeY=24
snakeX=24
snake_color1="red"
snake_color2="blue"
snakeMoveY=0
snakeMoveX=0
root=Tk()
root.title("змейка")
root.resizable(0,0)
canvas = Canvas(root, width=gameWidth, height=gameHeight,bd=0, highlightthickness=0)
canvas.pack()
root.update()
def snakePaint(canva, x, y):
    id1 = canva.create_rectangle(x * snake_item, y * snake_item, x * snake_item + snake_item,
                                  y * snake_item + snake_item, fill=snake_color2)
    id2 = canva.create_rectangle(x * snake_item + 2, y * snake_item + 2, x * snake_item + snake_item - 2,
                                  y * snake_item + snake_item - 2, fill=snake_color1)

def snakeMove(event):
    global snakeX
    global snakeY
    if event.keysym == "Up":
        snakeMoveY = -1
        snakeMoveX = 0
    if event.keysym == "Down":
        snakeMoveY = 1
        snakeMoveX = 0
    if event.keysym == "Left":
        snakeMoveY = 0
        snakeMoveX = -1
    if event.keysym == "Right":
        snakeMoveY = 0
        snakeMoveX = 1
    snakeX=snakeX + snakeMoveX
    snakeY=snakeY + snakeMoveY
    snakePaint(canvas,snakeX,snakeY)
canvas.bind_all("<KeyPress-Left>",snakeMove)
canvas.bind_all("<KeyPress-Right>",snakeMove)
canvas.bind_all("<KeyPress-Up>",snakeMove)
canvas.bind_all("<KeyPress-Down>",snakeMove)
root.mainloop()
